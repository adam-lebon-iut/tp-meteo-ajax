# TP3 - Application Web météo

## Version en ligne
Vous trouverez une version en ligne de cette application à l'adresse: https://meteo.tropy.land

## Principe de fonctionnement
- Une fois le formulaire soumis une première requête est effectuée auprès de l'API openweathermap
- La météo de la ville est récupérée sur 1 semaine et affichée dans un tableau
- En parallèle une seconde requête est effectué auprès de l'API Google Places afin de récupérer une image en fond de page

## Compatibilité
Beaucoup des fonctionnalités utilisées dans notre javascript sont issues des dernières normes (ES2015/ES6)
comme les fonctions fléchées ou les promesses. L'application n'est donc compatible qu'avec les derniers
navigateurs (chrome, firefox, opera, safari et edge). Aucune version d'Internet Explorer n'est prise
en charge car il ne gère et ne gèrera jamais les fonctions fléchées.
