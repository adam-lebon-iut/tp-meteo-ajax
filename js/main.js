class Application {

    /**
     * Constructeur de l'application météo
     * Stocke les référence des balises HTML générés dynamiquement
     */
    constructor() {
        /** ELEMENTS HTML */
        this.bgOverlay = document.querySelector('#bgOverlay');
        this.credits = document.querySelector('#credits');
        this.searchInput = document.querySelector('#searchInput');
        this.submitButton = document.querySelector('#searchForm button[type=submit]');
        this.cityTitle = document.querySelector('#cityTitle');
        this.meteo = document.querySelector('#meteo');
        
        /** Instance de l'accordéon de la météo */
        this.collapsible = M.Collapsible.init(this.meteo);

        /**
         * @var {boolean} requestInProgress Vaut true si une requête est déja en cours
         */
        this.requestInProgress = false;
    }

    /**
     * Effectue une requête AJAX du type GET et retourne le résultat dans une promesse
     * @param {string} url Url à appeller
     * @param {Object} params Paramètres GET à passer dans l'url
     * @return {string} Réponse de la requête HTTP sous forme de texte
     */
    async get(url, params) {
        if (params) {
            url += '?' + Object.keys(params).map((key) => key + '=' + encodeURIComponent(params[key])).join('&');
        }
        console.log(url);

        return new Promise((resolve, reject) => {
            const request = new XMLHttpRequest();
            request.open('GET', url);
            request.onreadystatechange = () => {
                if (request.readyState === XMLHttpRequest.DONE) {
                    if (request.status === 200) {
                        resolve(request.responseText);
                    } else {
                        reject(request.status);
                    }
                }
            };
            request.send(null);
        });
    }

    /**
     * Effectue une requête ajax pour récupéré les informations météo
     * en fonction de la ville entré dans l'input de recherche
     * Appelle ensuite les fonction fetchImage() et render() si la requête est un succès
     */
    async search() {
        if (this.requestInProgress) {
            return;
        }

        const searchRequest = this.searchInput.value;

        this.submitButton.classList.add('disabled');
        this.requestInProgress = true;

        try {
            const data = JSON.parse(await this.get('https://api.openweathermap.org/data/2.5/forecast/daily', {
                q: searchRequest.toLowerCase(),
                APPID: 'ee07e2bf337034f905cde0bdedae3db8',
                units: 'metric',
                lang: 'fr'
            }));

            this.render(data);
            this.fetchImage(data.city.name, data.city.country);
            this.searchInput.blur();
        } catch (error) {
            if (error === 404) {
                M.toast({ displayLength: 4000, html: 'Cette ville est introuvable 🤨' });
            } else {
                M.toast({ displayLength: 4000, html: 'Impossible d\'effectuer la requête 😬<br>Vérifiez votre connexion internet' });
            }
        } finally {
            this.submitButton.classList.remove('disabled');
            this.requestInProgress = false;
        }
    }

    /**
     * Utilise l'API google maps places afin d'afficher une image de la ville spécifié
     * en fond de page
     * @param {string} city Nom de la ville à rechercher
     * @param {string} country Nom du pays de la ville, pour plus de précision lors de la recherche
     */
    async fetchImage(city, country) {
        
        new google.maps.places.PlacesService(document.createElement('div'))
            .findPlaceFromQuery(
                { query: `${city}, ${isoCountries[country]}`, fields: ['photos'] },
                (result) => {
                    console.log(result);
                    document.querySelector('body').style.backgroundImage = `url(${result[0].photos[0].getUrl()})`;
                    this.credits.innerHTML = `Image by ${result[0].photos[0].html_attributions[0]}`;
                    this.credits.style.display = 'inline-block';
                }
            );
    }

    /**
     * Génère le rendu HTML de la div météo en fonction des données passées en paramètre
     * 
     * @param {Array<*>} data Données météo
     */
    render(dataList) {
        console.log(dataList);

        // Nom de la ville
        this.cityTitle.querySelector('h1').innerText = dataList.city.name;
        // Nom du pays
        this.cityTitle.querySelector('h2').innerText = isoCountries[dataList.city.country];

        // Tableau météo
        const render = dataList.list.map((data, i) => {
            let date = new Date(data.dt * 1000);
            date = date.toLocaleDateString('fr-FR', { weekday: 'long', day: 'numeric', month: 'long' });

            return `
                <li>
                    <div class="collapsible-header">
                        <i class="wi wi-owm-${data.weather[0].id}"></i>${date}
                        <span class="right"><strong>${data.temp.day}°C</strong></span>
                    </div>
                    
                    <div class="collapsible-body">
                        <span>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <i class="wi wi-owm-${data.weather[0].id}"></i>
                                            Météo
                                        </td>
                                        <td>
                                            ${data.weather[0].description}
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="header">
                                            <i class="wi wi-thermometer"></i>
                                            Température
                                        </td>
                                        <td>
                                            <strong>Minimum:</strong> ${data.temp.min}°C
                                            &nbsp;
                                            <strong>Moyen:</strong> ${data.temp.day}°C
                                            &nbsp;
                                            <strong>Maximum:</strong> ${data.temp.max}°C
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <i class="wi wi-humidity"></i>
                                            Humidité
                                        </td>

                                        <td>
                                            ${data.humidity}%
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <i class="wi wi-strong-wind"></i>
                                            Vitesse du vent
                                        </td>

                                        <td>
                                            ${data.speed} Km/h
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <i class="wi wi-barometer"></i>
                                            Pression atmosphérique
                                        </td>

                                        <td>
                                            ${data.pressure} hPa
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </span>
                    </div>
                </li>
            `;
        }).join('\n');

        this.meteo.innerHTML = render;

        // On ouvre la météo du jour courant
        this.collapsible.open(0);
    }
}
